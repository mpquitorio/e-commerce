<?php

    include_once "../config/dbh.inc.php";

    $productId          = mysqli_real_escape_string($conn, $_POST['product_id']);
    $productName        = mysqli_real_escape_string($conn, $_POST['name']);
    $productPrice       = mysqli_real_escape_string($conn, $_POST['price']);
    $productDescription = mysqli_real_escape_string($conn, $_POST['description']);
    $imgPath            = mysqli_real_escape_string($conn, $_POST['img_path']);
    $categoryId         = mysqli_real_escape_string($conn, $_POST['category_id']);

    $sql = "UPDATE `items` 
            SET name = '$productName', price = '$productPrice', description = '$productDescription', img_path = '$imgPath', category_id = '$categoryId' 
            WHERE id = '$productId' ";

    $result = mysqli_query($conn, $sql);

    if( $result ){
        header('location: ../admin/products.php?message=updateSuccess');
    }else{
        header('location: ../admin/products.php?message=updateFailed');
    }
    
?>