<?php

    include_once "../config/dbh.inc.php";

    $productName        = mysqli_real_escape_string($conn, $_POST['name']);
    $productPrice       = mysqli_real_escape_string($conn, $_POST['price']);
    $productDescription = mysqli_real_escape_string($conn, $_POST['description']);
    $categoryId         = $_POST['category_id'];
    
    $productFile        = $_FILES['file'];
    //print_r($productFile);
    
    $fileName = $_FILES['file']['name'];
    $fileTmpName = $_FILES['file']['tmp_name'];
    $fileSize = $_FILES['file']['size'];
    $fileError = $_FILES['file']['error'];
    $fileType = $_FILES['file']['type'];

    $fileExt = explode('.', $fileName);
    $fileActualExt = strtolower(end($fileExt));

    $allowedExt = array('jpg', 'jpeg', 'png', 'gif');

    if ( in_array($fileActualExt, $allowedExt) ){
        if ( $fileError === 0 ){
            if($fileSize < 1000000){
                $fileNameNew = uniqid('', true) . "." . $fileActualExt;
                $fileDestination = "../assets/images/" . $fileNameNew;
                move_uploaded_file($fileTmpName, $fileDestination);

                $sql = "INSERT INTO `items` (name, price, description, img_path, category_id) VALUES ('$productName', '$productPrice','$productDescription', '$fileDestination', '$categoryId')";

                $result = mysqli_query($conn, $sql);

                if( $result ){
                    header('location: ../admin/home.php?message=addSuccess');
                }else{
                    header('location: ../admin/home.php?message=addFailed');
                }
            }else{
                header('location: ../admin/home.php?message=fileSize');
            }
        }else{
            header('location: ../admin/home.php?message=fileError');
        }
    }else{
        header('location: ../admin/home.php?message=fileExtension');
    }

?>