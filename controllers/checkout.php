<?php
    session_start();

    if( !isset($_SESSION['user_id']) ){
        header("location: ../views/cart.php?checkout=login");
    }else{
    	header("location: ../views/payment.php");
    }

?>
