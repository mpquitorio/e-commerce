<?php

    $dbServername = "localhost";
    $dbUsername   = "root";
    $dbPassword   = "";
    $dbName       = "demoStoreNew";

    /* create connection */
    $conn = mysqli_connect($dbServername, $dbUsername, $dbPassword, $dbName);

    /* check connection */
    if( ! $conn ){
        echo "Error: Unable to connect to MySQL. " . mysqli_connect_error();
    }

?>