<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top">
    <div class="container">
        <a class="navbar-brand" href="home.php"><i class="fab fa-buysellads fa-2x"></i> DEMO SHOP</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="nav-link" href="catalog.php"><i class="fas fa-book"></i>&nbsp;Catalog</a>
                </li>
                <li class="nav-item"> 
                     <a class="nav-link" href="cart.php"><i class="fas fa-shopping-cart"></i>&nbsp;Cart <span class="badge badge-pill badge-info"><?php isset($_SESSION["cart"]) ? print(count($_SESSION["cart"])) : ""; ?></span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#"><i class="fas fa-info-circle"></i>&nbsp;About Us</a>
                </li>
                <?php
                    if( isset($_SESSION['user_id']) )
                    {
                      echo '<li class="nav-item">
                                <a class="nav-link active">Welcome <span class="text-lowercase">' . $_SESSION['username'] .'</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="../controllers/logout.php"><i class="fas fa-sign-out-alt"></i>&nbsp;Logout</a>
                            </li>';
                    }
                    else
                    {
                      echo '<li class="nav-item">
                                <a class="nav-link" href="login.php"><i class="fas fa-user-lock"></i>&nbsp;Login</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="register.php"><i class="fas fa-user-plus"></i>&nbsp;Register</a>
                            </li>';
                    }
                ?>
                
            </ul>
        </div>
    </div>
</nav>