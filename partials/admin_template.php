<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Meta -->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <!-- Title -->
        <title><?php echo $title; ?></title>

        <!-- Bootswatch -->
        <link href="../assets/css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="../assets/css/style.css" rel="stylesheet">
        <!-- Fontawesome -->
        <script src="https://kit.fontawesome.com/54dd48c648.js" crossorigin="anonymous"></script>

    </head>
    <body>
        <?php 
            if( $title == "Admin") { 

                get_content(); 
                require_once "footer.php";

            }else{

                require_once "admin_navbar.php";
                get_content();
                require_once "footer.php";

            }
        ?>

        <!-- jQuery -->
        <script src="../assets/js/jquery-3.3.1.min.js"></script>
        <!-- Popper -->
        <script src="../assets/js/popper.min.js"></script>
        <!-- Bootstrap JS -->
        <script src="../assets/js/bootstrap.min.js"></script>
    </body>
</html>