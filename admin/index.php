<?php $title = "Admin"; ?>

<?php require_once "../partials/admin_template.php"; ?>

<?php function get_content(){  ?>

    <main style="min-height: 73vh;">
        <div class="container">
            <div class="row">
                <div class="col col-md-8 col-sm-12 m-auto">
                    <h1 class="text-center mb-5"><i class="fab fa-buysellads fa-2x"></i><br>DEMO SHOP</h1>
                    <fieldset>    	
                        <legend>ADMIN LOGIN</legend>
                        <?php
                            if( isset( $_GET['message'] ) ){

                                $response = $_GET['message'];

                                if ( $response == "failed" ){
                                    $response = "<div class='alert alert-warning alert-dismissible fade show' role='alert'><i class='fas fa-exclamation-triangle'></i>&nbsp;Invalid Username and/or Password. 
                                                    <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                                        <span aria-hidden='true'>&times;</span>
                                                    </button>
                                                </div>";
                                }

                                if ( $response == "denied" ){
                                    $response = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><i class='fas fa-exclamation-triangle'></i>&nbsp;Access Denied! 
                                                    <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                                        <span aria-hidden='true'>&times;</span>
                                                    </button>
                                                </div>";
                                }

                            }else{

                                $response = "";

                            }
                        ?>

                        <?php echo $response; ?>

                        <form action="../controllers/admin_index_action.php" method="POST">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fas fa-user"></i></div>
                                </div>
                                <input type="text" class="form-control" name="username" placeholder="Username" required>
                            </div>
                            <br>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fas fa-lock"></i></div>
                                </div>
                                <input type="password" class="form-control" name="password" placeholder="Password" required>
                            </div>
                            <br>
                            <button type="submit" name="submit" class="btn btn-block btn-primary"><i class="fas fa-user-lock"></i> &nbsp;LOG IN</button>
                        </form>
                    </fieldset>	
                </div>
            </div>	
        </div>
    </main>

<?php } ?>