<?php 
    $title = "Home";

    session_start();

    if( !isset($_SESSION['uid']) ){
        header('location: ../admin/index.php?message=denied');
    }
?>

<?php require_once "../partials/admin_template.php"; ?>

<?php function get_content(){  ?>

    <main>
        <div class="container" style="padding: 50px 10px;">
            <div class="row">
                <div class="col col-sm-12 col-md-6 col-lg-4">
                    <fieldset style="height: 100px; background-color:#f5f5f5;">
                        <legend>MENU</legend>
                        <div class="list-group">
                            <a href="products.php" class="list-group-item list-group-item-action"><i class="fas fa-list"></i>&nbsp;View Product List</a>
                        </div>
                    </fieldset>
                </div>

                <div class="col col-sm-12 col-md-6 col-lg-8">
                    <fieldset >
                        <legend>ADD NEW PRODUCT</legend>
                        <?php
                            if( isset( $_GET['message'] ) ){

                                $response = $_GET['message'];

                                if ( $response == "addSuccess" ){
                                    $response = "<div class='alert alert-success alert-dismissible fade show' role='alert'><i class='fas fa-info-circle'></i>&nbsp;Successfully Added!
                                                    <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                                        <span aria-hidden='true'>&times;</span>
                                                    </button>
                                                </div>";
                                }

                                if ( $response == "addFailed" ){
                                    $response = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><i class='fas fa-info-circle'></i>&nbsp;Failed to Add Product. 
                                                    <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                                        <span aria-hidden='true'>&times;</span>
                                                    </button>
                                                </div>";
                                }

                                if ( $response == "fileExtension" ){
                                    $response = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><i class='fas fa-info-circle'></i>&nbsp;You cannot upload files! Allowed only are .jpg, .jpeg, .png, and .gif file extension. 
                                                    <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                                        <span aria-hidden='true'>&times;</span>
                                                    </button>
                                                </div>";
                                }

                                if ( $response == "fileError" ){
                                    $response = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><i class='fas fa-info-circle'></i>&nbsp;There was an error uploading your file!
                                                    <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                                        <span aria-hidden='true'>&times;</span>
                                                    </button>
                                                </div>";
                                }

                                if ( $response == "fileSize" ){
                                    $response = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><i class='fas fa-info-circle'></i>&nbsp;Your file is too big! Allowed only is less than 1MB. 
                                                    <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                                        <span aria-hidden='true'>&times;</span>
                                                    </button>
                                                </div>";
                                }

                            }else{

                                $response = "";

                            }
                        ?>

                        <?php echo $response; ?>

                        <form action="../controllers/add_product.php" method="POST" enctype="multipart/form-data">
                            <input type="text" class="form-control" name="name" placeholder="Product Name" required>
                            <br>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">₱</div>
                                </div>
                                <input type="number" class="form-control" name="price" placeholder="Item Price" required>
                            </div>
                            <br>
                            <select name="category_id" class="form-control">
                                <option disabled selected>Select Category</option>
                                <?php
                                    require_once "../config/dbh.inc.php";

                                    $sql = "SELECT * FROM `categories`";
                                    $result = mysqli_query($conn, $sql);

                                    while ( $row = mysqli_fetch_assoc($result) ) {
                                        echo "<option value='$row[id]'>$row[name]</option>";
                                    }
                                ?>
                            </select>
                            <br>
                            <textarea class="form-control" rows="10" name="description" placeholder="Description"></textarea>
                            <br>
                            <input type="file" name="file" class="form-control">
                            <br>
                            <button type="submit" name="submit" class="btn btn-block btn-primary">Submit</button>
                        </form>
                    </fieldset>
                </div>
            </div>	
        </div>
    </main>

<?php } ?>