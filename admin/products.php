<?php 

    $title = "Products";

    session_start();

    if( !isset($_SESSION['uid']) ){
        header('location: ../admin/index.php?message=denied');
    }

?>

<?php require_once "../partials/admin_template.php"; ?>

<?php function get_content(){  ?>

    <main style="min-height: 73vh;">
    
        <div class="container" style="padding: 50px 10px;">
            <div class="row">
                <div class="col col-sm-12 col-md-6 col-lg-3">
                    <fieldset style="min-height: 100px; background-color:#f5f5f5;">
                        <legend>CATEGORY</legend>
                        <div class="list-group">
                            <a href="products.php" class="list-group-item list-group-item-action font-weight-bold ">ALL PRODUCTS</a>
                            <?php
                                include_once "../config/dbh.inc.php";
                                
                                $sql = "SELECT * FROM `categories`";
                                $result = mysqli_query($conn, $sql);

                                if( mysqli_num_rows($result) > 0 ){
                                    while( $row = mysqli_fetch_assoc($result) ){
                                        echo "<a href='products.php?id=$row[id]' class='list-group-item list-group-item-action'><i class='fas fa-chevron-circle-right'></i>&nbsp;$row[name]</a>";
                                    }
                                }
                                
                            ?>
                        </div>
                    </fieldset>
                </div>
                <div class="col col-sm-12 col-md-6 col-lg-9">
                    <fieldset>  
                        <legend>PRODUCT LISTS</legend>  
                        <?php
                            if( isset( $_GET['message'] ) ){

                                $response = $_GET['message'];

                                if ( $response == "deleteSuccess" ){
                                    $response = "<div class='alert alert-success alert-dismissible fade show' role='alert'>Successfully deleted. 
                                                    <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                                        <span aria-hidden='true'>&times;</span>
                                                    </button>
                                                </div>";
                                }

                                if ( $response == "deleteFailed" ){
                                    $response = "<div class='alert alert-danger alert-dismissible fade show' role='alert'>Failed to delete the product. 
                                                    <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                                        <span aria-hidden='true'>&times;</span>
                                                    </button>
                                                </div>";
                                }

                                if ( $response == "updateSuccess" ){
                                    $response = "<div class='alert alert-success alert-dismissible fade show' role='alert'>Successfully updated. 
                                                    <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                                        <span aria-hidden='true'>&times;</span>
                                                    </button>
                                                </div>";
                                }

                                if ( $response == "updateFailed" ){
                                    $response = "<div class='alert alert-danger alert-dismissible fade show' role='alert'>Failed to update the product. 
                                                    <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                                        <span aria-hidden='true'>&times;</span>
                                                    </button>
                                                </div>";
                                }

                            }else{

                                $response = "";

                            }

                        ?>

                        <?php echo $response;  ?>

                        <div class="table-responsive">
                            <?php

                                if( isset( $_GET['id'] ) ){
                                    $cat_id = $_GET['id'];
                                    $sql = "SELECT * FROM `items` WHERE category_id = '$cat_id'";
                                }else{
                                    $sql = "SELECT * FROM `items`";
                                }
                
                                $result = mysqli_query($conn, $sql);

                            ?>
                
                            <table class="table">
                                <thead>
                                    <tr class="text-center bg-primary text-white" style="padding: 0px">
                                        <th width="5%" style="font-size: 10px; padding: 15px 5px">No.</th>
                                        <th width="25%" style="font-size: 10px; padding: 15px 5px">Product Image</th>
                                        <th width="60%" style="font-size: 10px; padding: 15px 5px">Particular</th>
                                        <th width="10%" style="font-size: 10px; padding: 15px 5px">Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                            <?php
                                if ( mysqli_num_rows($result) > 0 ) {

                                    $count = 1;
                                    while( $row = mysqli_fetch_assoc($result) ){
                            
                            ?>
                                       
                                    <tr>
                                        <td width="5%" style="padding: 10px"><?php echo $count ?></td>
                                        <td width="25%" style="padding: 10px"><img src="<?php echo $row['img_path'] ?>" style="width: 100%; height: 180px; border: 1px solid #ddd"></td>
                                        <td width="60%" style="font-size: 12px; padding: 10px; letter-spacing: 0px;" class="text-justify">
                                            <strong class="font-weight-bold">Name:</strong> <?php echo $row['name'] ?><br><hr>
                                            <strong class="font-weight-bold">Price: ₱</strong> <?php echo $row['price'] ?><br><hr>
                                            <strong class="font-weight-bold">Description:</strong> <?php echo $row['description'] ?>
                                        </td>
                                        <td width="10%" class="text-center" style="padding: 10px;">
                                            <button class="btn text-success p-1" data-toggle="modal" data-target="#editProduct<?php echo $row['id'] ?>"><i class="fas fa-edit" style="font-size: 15px"></i></button> 
                                            <a class="btn text-danger p-1" href="../controllers/delete_product.php?id=<?php echo $row['id'] ?>"><i class="fas fa-trash-alt" style="font-size: 15px"></i></a> 
                                        </td>
                                    </tr>

                            <?php
                                        $count++;
                            ?>

                                    <div class="modal fade" id="editProduct<?php echo $row['id'] ?>" aria-hidden="true">
                                        <div class="modal-dialog modal-md">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <fieldset >
                                                                <legend>UPDATE PRODUCT</legend>
                                                                <form method="POST" action="../controllers/edit_product_action.php" class="mt-3">
                                                                    <input type="hidden" name="product_id" value="<?php echo $row['id'] ?>">
                                                                    <label for="name" class="font-weight-bold" style="letter-spacing: 0px;">Product Name:</label>
                                                                    <input type="text" class="form-control" name="name" id="name" value="<?php echo $row['name'] ?>" required>
                                                                    <br>
                                                                    <label for="price" class="font-weight-bold" style="letter-spacing: 0px;">Product Price:</label>
                                                                    <div class="input-group">
                                                                        <div class="input-group-prepend">
                                                                            <div class="input-group-text">₱</div>
                                                                        </div>
                                                                        <input type="number" class="form-control" name="price" id="price" value="<?php echo $row['price'] ?>" required>
                                                                    </div>
                                                                    <br>
                                                                    <label for="description" class="font-weight-bold" style="letter-spacing: 0px;">Product Description:</label>
                                                                    <textarea class="form-control" rows="5" name="description" id="description"><?php echo $row['description'] ?></textarea>
                                                                    <br>
                                                                    <label for="img_path" class="font-weight-bold" style="letter-spacing: 0px;">Image URL:</label>
                                                                    <input type="text" class="form-control" name="img_path" id="img_path" value="<?php echo $row['img_path'] ?>" required>
                                                                    <br>
                                                                    <label for="category_id" class="font-weight-bold" style="letter-spacing: 0px;">Product Category</label>
                                                                    <select name="category_id" id="category_id" class="form-control">

                                                                    <?php

                                                                    $sql_2 = "SELECT * FROM `categories` WHERE id='$row[category_id]'";
                                                                    $result_2 = mysqli_query($conn, $sql_2);

                                                                    while( $row_2 = mysqli_fetch_assoc($result_2) ) {

                                                                    ?>

                                                                        <option selected value="<?php echo $row_2['id'] ?>"><?php echo $row_2['name'] ?></option>

                                                                    <?php

                                                                        $sql_3 = "SELECT * FROM `categories` WHERE id <> '$row[category_id]' ";
                                                                        $result_3 = mysqli_query($conn, $sql_3);

                                                                        while( $row_3 = mysqli_fetch_assoc($result_3) ) {

                                                                    ?>

                                                                        <option value="<?php echo $row_3['id'] ?>" class="font-weight-bold"><?php echo $row_3['name'] ?></option>';
                                                                    
                                                                    <?php
                                                                        }
                                                                    }

                                                                    ?>

                                                                    </select>
                                                                    <br>
                                                                    <button type="submit" name="submit" class="btn btn-block btn-primary">Update</button>
                                                                </form
                                                            </fieldset>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                            <?php        
                                    }
                                }else{
                            
                            ?>
                                    <tr>
                                        <td colspan="4" class="text-center">*** No Records Found ***</td>
                                    </tr>
                            <?php
                                }

                            ?>
                                </tbody>
                            </table>
                        </div>
                    </fieldset>	
                </div>	
            </div>	
        </div>
    </main>

<?php } ?>