<?php 
    session_start();

    if( !isset($_SESSION['user_id']) ){
        header('location: ../views/login.php?message=denied');
    }


    $title = "Payment"; 
    require_once "../partials/template.php";
?>


<?php function get_content(){  ?>

	<main>
		<div class="container mt-5 mb-5">
			<div class="row">
				<div class="col-md-6">
					<fieldset>    	
                        <legend>Billing Information</legend>
                        <input type="text" class="form-control border border-secondary mb-1" name="fname" placeholder="First Name" required>
                        <input type="text" class="form-control border border-secondary mb-1" name="lname" placeholder="Last Name" required>
                        <input type="email" class="form-control border border-secondary mb-1" name="email" placeholder="Email Address" required><br>
                        <input type="text" class="form-control border border-secondary mb-1" name="address" placeholder="Street" required>
                        <input type="text" class="form-control border border-secondary mb-1" name="city" placeholder="City" required>
                        <input type="text" class="form-control border border-secondary mb-1" name="postal" placeholder="Zip/Postal Code" required>
                        <select class="form-control border border-secondary mb-1" name="country">
                        	<option value="PH">Philippines</option>
                        	<option value="USA">United States of America</option>
                        </select>
                        <input type="number" class="form-control border border-secondary" name="contact" placeholder="Phone" required>
	                </fieldset>  
	                <fieldset class="mt-3">    	
                        <legend>Payment Info</legend>
                        <img src="https://i.ya-webdesign.com/images/we-accept-credit-cards-png-8.png" width="200px" class="float-right mb-2" style="margin-top: -20px">
                        <input type="number" class="form-control border border-secondary mb-1" name="credit_card" placeholder="Credit Card Number" required>
                        <div class="input-group mb-1">
                        	<div class="input-group-prepend">
							    <span class="input-group-text">Valid until</span>
							 </div>
						  	<select class="custom-select form-control border border-secondary">
						    	<?php
									for($month = 1; $month <= 12; ++$month) {
										$month_label = date('F', mktime(0, 0, 0, $month, 1));
    									echo '<option value="'. $month_label .'">'. $month_label .'</option>';
									} 
								?>
						  	</select>
						  	<select class="custom-select form-control border border-secondary">
						    	<?php 
						          	$year = date('Y');
						          	$min = $year - 60;
						          	$max = $year + 10;
						          	for( $i = $max; $i >= $min; $i-- ) {
						            	echo '<option value="'. $i .'">'. $i .'</option>';
						          	}
						        ?>
						  	</select>
						</div>
						<input type="number" class="form-control border border-secondary" name="cvc" placeholder="CVC" required>
	                </fieldset>  
				</div>
				<div class="col-md-6">
					<fieldset>    	
                        <legend>Your Cart</legend>

					<?php
						if( isset($_SESSION["cart"]) )
                  		{

                  		echo '<div class="table-responsive m-auto bg-secondary">
                                 <table class="table">
                                    <thead>
                                         <tr class="bg-primary text-white text-center" style="padding: 0px">
                                             <th width="2%" style="font-size: 10px; padding: 15px 5px">No</th>
                                             <th width="23%" style="font-size: 10px; padding: 15px 5px">Image</th>
                                             <th width="40%" style="font-size: 10px; padding: 15px 5px">Product</th>
                                             <th width="5%" style="font-size: 10px; padding: 15px 5px">Qty</th>
                                             <th width="30%" style="font-size: 10px; padding: 15px 5px">Sub Total</th>
                                         </tr>
                                    </thead>
                                    <tbody>';

                       		 include_once "../config/dbh.inc.php";

		                     $sql = "SELECT * FROM `items`";
		                     $result = mysqli_query($conn, $sql);
		   
		                     $total = 0; 
		                     $count = 1;
		                     while ( $row = mysqli_fetch_assoc($result) ){
		                        foreach ($_SESSION["cart"] as $key => $value) {
		                           if( $row["id"] == $value["product_id"] )
		                           {

		                              echo '<tr>
		                                       <td width="2%" class="p-2" align="center">' . $count .'</td>
		                                       <td width="23%" class="p-2" style="padding: 0px"><img src="' . $row["img_path"] .'" alt="" width="100%" height="80px" style="border: 1px solid #ddd">
		                                       </td>
		                                       <td width="40%" class="p-2" align="left" style="font-size: 12px; padding: 10px; letter-spacing: 0px;">' . $row["name"] .'       
		                                       </td>
		                                       <td width="5%" class="p-2" align="right"><input type="number" class="pl-2 qty" min="1" max="10" data-product-pos="' . $key. '" data-product-id="'. $value["product_id"] .'" data-price="'. $row["price"] .'" value="' . $value["quantity"] .'" disabled>
		                                       </td>
		                                       <td width="30%" class="p-2" align="right">₱ <span class="sub">' . number_format($value["quantity"] * $row["price"], 2) .'</span>
		                                       </td>
		                                    </tr>';

		                              $total += ( $value["quantity"] * $row["price"] );
		                              $count++;
		                           }
		                        }
		                     }

		                    	echo '<tr>
			                    		<td colspan="5" class="p-1"></td>
			                    	 </tr>
			                    	 <tr align="right" class="font-weight-bold" style="font-size:15px">
			                    		<td colspan="4" class="p-2">Total</td>
			                    		<td class="p-2">₱ <span id="subtotal">' . number_format($total,2) .'</span></td>
			                    	 </tr>';

                              echo '</tbody>
                                 </table>
                              </div>';

                        }
					?>
					</fieldset>
					<p class="mt-3 text-center"><a href="" class="btn btn-primary">REVIEW ORDER</a></p>
				</div>
			</div>
		</div>
	</main>

<?php } ?>