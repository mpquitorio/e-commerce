<?php 
    session_start();
    
    $title = "Home"; 
    include_once "../partials/template.php";
?>

<?php function get_content(){ ?>

    <main>
        <div class="jumbotron">
            <div class="container">
                <h1 class="display-3">The Demo Shop</h1>
                <p>
                    This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something unique. 
                </p>
                <p>
                    <a href="catalog.php" class="btn btn-outline-primary">Shop Now</a>
                </p>
            </div>
        </div>

        <div class="container">
            <h2>Featured Items</h2>
            <hr>
            <div class="row text-center">

                <?php

                    include_once "../config/dbh.inc.php";

                    $sql = "SELECT * FROM `items` limit 4";
                    $result = mysqli_query($conn, $sql);
                    
                    if( mysqli_num_rows($result) > 0){

                        while( $row = mysqli_fetch_assoc($result) ){
                        echo "  
                            <div class='col-md-3 mb-3'>
                                <div class='card h-100'>
                                    <img src='$row[img_path]' alt=''>
                                    <div class='card-body'>
                                        <h4 class='card-title'>" . substr($row['name'],0,15). "</h4>
                                        <h5>₱ $row[price]</h5>
                                        <a href='product.php?id=$row[id]' class='btn btn-block btn-primary'>View Product</a>
                                    </div>
                                </div>
                            </div>
                            ";
                        }
                    }
                ?>

            </div>
        </div>
    </main>

<?php } ?>
