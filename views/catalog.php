<?php 
    session_start();

    if ( isset($_POST["submit"]) )
    {
        if( isset($_SESSION["cart"]) )
        {
            $item_array_id = array_column($_SESSION["cart"], 'product_id');

            if ( !in_array($_GET["cat_id"], $item_array_id) )
            {
                $_SESSION["cart"][] = array(
                    'quantity'   => $_POST["qty"],
                    'product_id' => $_GET["cat_id"]
                );
            } 
            else
            {  
                echo '<script>alert("Item already added to cart!")</script>';       
            }
        }
        else
        {
            $_SESSION["cart"][0] = array(
                'quantity'   => $_POST["qty"],
                'product_id' => $_GET["cat_id"]
            );  
        }
    }
    
    $title = "Catalog"; 
    include_once "../partials/template.php";
    
?>

<?php function get_content(){ ?>

    <main>
        <div class="container mt-5">
            <div class="row">
                <!-- Catalogue -->
                <div class="col-lg-2">
                    <h4 id="catalogue">Collection</h4>
                    <div class="list-group">
                        <?php
                            include_once "../config/dbh.inc.php";
                            
                            $sql = "SELECT * FROM `categories`";
                            $result = mysqli_query($conn, $sql);

                            if( mysqli_num_rows($result) > 0 ){
                                while( $row = mysqli_fetch_assoc($result) ){
                        ?>
                                    <a href="catalog.php?id=<?php echo $row['id']; ?>" class="list-group-item list-group-item-action"><i class="fas fa-chevron-circle-right"></i>&nbsp;<?php echo $row['name']; ?></a>
                        <?php       
                                }
                            }
                        ?>
                    </div>
                </div>

                <!-- Search Box -->
                <div class="col-lg-10">
                    <div class="form-group">
                        <div class="form-group">
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" id="search">
                                <div class="input-group-append">
                                    <span class="input-group-text"><i class="fas fa-search"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <?php
                            if( isset( $_GET['id'] ) ){
                                $cat_id = $_GET['id'];
                                $sql = "SELECT * FROM `items` WHERE category_id = '$cat_id'";
                            }else{
                                $sql = "SELECT * FROM `items`";
                            }

                            $result = mysqli_query($conn, $sql);
                            
                            if( mysqli_num_rows($result) > 0 ){
                                while( $row = mysqli_fetch_assoc($result) ){
                        ?>
                                    <div class="col col-lg-4 mb-3 d-inline-block">
                                        <div class="card h-100">
                                            <img src="<?php echo $row['img_path']; ?>" width="100%" height="250px" style="border-bottom: 1px solid #ddd">
                                            <div class="card-body">
                                                <a href="product.php?id=<?php echo $row['id']; ?>"><h4 class="card-title"><?php echo substr($row['name'],0,15); ?></h4></a>
                                                <h5>₱ <?php echo $row['price']; ?></h5>
                                                <a href="product.php?id=<?php echo $row['id']; ?>" class="btn btn-block btn-primary"> <i class="fas fa-shopping-cart"></i>&nbsp;Buy Now</a>
                                            </div>
                                        </div>
                                    </div>
                        <?php
                                }
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </main>

<?php } ?>