<?php session_start(); ?>

<?php
# =============================================
# =        Removing Product from Cart         =
# =============================================

   if( isset($_GET["product_id"]) )
   {
      foreach($_SESSION["cart"] as $key => $value)
      {
         if($value["product_id"] == $_GET["product_id"])
         {
            unset( $_SESSION["cart"][$key] );
         }
         if( empty($_SESSION["cart"]) )
         {
            unset( $_SESSION["cart"] );
         } 
      }
   }

# ===   End of Removing Product from Cart   ===
?>

<?php 
   $title = "Cart";
   include_once "../partials/template.php";
?>

<?php function get_content(){ ?>

   <main style="min-height: 66.5vh">
      <div class="container mt-5 mb-5">
         <div class="row">
               <?php

                  if( isset($_SESSION["cart"]) )
                  {

                     echo '<div class="col-sm-12 col-md-8"> 
                              <div class="table-responsive m-auto">
                                 <h1>Your Cart</h1>
                                 <table class="table table-hover">
                                    <thead>
                                         <tr class="bg-primary text-white text-center" style="padding: 0px">
                                             <th width="2%" style="font-size: 10px; padding: 15px 5px">No</th>
                                             <th width="13%" style="font-size: 10px; padding: 15px 5px">Image</th>
                                             <th width="60%" style="font-size: 10px; padding: 15px 5px">Particular</th>
                                             <th width="5%" style="font-size: 10px; padding: 15px 5px">Qty</th>
                                             <th width="15%" style="font-size: 10px; padding: 15px 5px">Sub Total</th>
                                             <th width="5%" style="font-size: 10px; padding: 15px 5px">Action</th>
                                         </tr>
                                    </thead>
                                    <tbody>';

                     include_once "../config/dbh.inc.php";

                     $sql = "SELECT * FROM `items`";
                     $result = mysqli_query($conn, $sql);
   
                     $total = 0; 
                     $count = 1;
                     while ( $row = mysqli_fetch_assoc($result) ){
                        foreach ($_SESSION["cart"] as $key => $value) {
                           if( $row["id"] == $value["product_id"] )
                           {

                              echo '<tr>
                                       <td width="2%" class="p-2" align="center">' . $count .'</td>
                                       <td width="13%" class="p-2" style="padding: 0px"><img src="' . $row["img_path"] .'" alt="" width="100%" height="70px" style="border: 1px solid #ddd">
                                       </td>
                                       <td width="60%" class="p-2 text-justify" align="left" style="font-size: 12px; padding: 10px; letter-spacing: 0px;">
                                          <strong class="font-weight-bold">Name: </strong>' . $row["name"] .'<br><hr>
                                          <strong class="font-weight-bold">Price: ₱ </strong>' . number_format($row["price"],2) .'<br><hr>
                                          <strong class="font-weight-bold">Description: </strong>' . substr($row["description"], 0, 150) .'...            
                                       </td>
                                       <td width="5%" class="p-2" align="right"><input type="number" class="pl-2 qty" min="1" max="10" data-product-pos="' . $key. '" data-product-id="'. $value["product_id"] .'" data-price="'. $row["price"] .'" value="' . $value["quantity"] .'">
                                       </td>
                                       <td width="15%" class="p-2" align="right">₱ <span class="sub">' . number_format($value["quantity"] * $row["price"], 2) .'</span>
                                       </td>
                                       <td width="5%" class="p-2" align="center"><a href="cart.php?product_id=' . $value["product_id"] .'"><span class="text-danger font-weight-bold" style="letter-spacing: 0px;"><i class="fas fa-trash-alt" style="font-size: 15px"></i></span></a>
                                       </td>
                                    </tr>';

                              $total += ( $value["quantity"] * $row["price"] );
                              $count++;
                           }
                        }
                     }

                              echo '</tbody>
                                 </table>
                              </div>
                           </div>';

                           echo '<div class="col-sm-12 col-md-4 mt-5">
                                      <div class="card">
                                          <div class="card-header">
                                              <h4>Order Summary</h4>
                                          </div>
                                          <div class="card-body">
                                              <h5>Subtotal: ₱ <span id="subtotal" class="display-5">' . number_format($total,2) .'</span></h5>
                                              <h5>Shipping:<span class="text-success">Free</span> </h5>
                                              <h5>Total: <span class="display-4"> ₱</span><span id="total" class="display-4">' . $total .'</span></h5>
                                          </div>
                                          <div class="card-footer">
                                              <a href="../controllers/checkout.php" class="btn btn-info btn-block">Checkout</a>
                                          </div>
                                      </div>
                                 </div>';

                           if( isset($_GET["checkout"]) )
                           {
                              $message = $_GET["checkout"];
                              if( $message == "login" ) 
                              {

                              echo '<script type="text/javascript">
                                       $(window).on("load", function() {
                                          $("#login-form").modal("show");
                                       });
                                    </script>';
                              }
                           }

                           echo '<div class="modal hide fade" id="login-form">
                                    <div class="modal-dialog">
                                        <div class="modal-content p-3">
                                            <div class="modal-header">
                                                <h5 class="modal-title">LOGIN</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <fieldset class="mt-2">
                                                <form action="../controllers/payment_login_action.php" method="POST">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <div class="input-group-text"><i class="fas fa-user"></i></div>
                                                        </div>
                                                        <input type="text" class="form-control" name="username" placeholder="Username" required>
                                                    </div>
                                                    <br>
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <div class="input-group-text"><i class="fas fa-lock"></i></div>
                                                        </div>
                                                        <input type="password" class="form-control" name="password" placeholder="Password" required>
                                                    </div>
                                                    <br>
                                                    <button type="submit" name="submit" class="btn btn-block btn-primary"><i class="fas fa-user-lock"></i> &nbsp;LOG IN</button>
                                                </form>
                                            </fieldset> 
                                        </div>
                                    </div>
                                 </div>';

                  }
                  else
                  {
               
                  echo '<div class="col-sm-12 col-md-12"> 
                           <h1 class="text-center">Your Cart</h1>
                           <hr>
                           <div class="text-center mt-5">
                              <h1><i class="fas fa-shopping-cart"></i></h1>
                              <h4>No products in the cart.</h4>
                              <small>Before proceed to checkout you must add some products to your shopping cart.<br> You will find a lot of interesting products on our "Catalog" page.</small>
                              <p class="mt-5"><a href="catalog.php" class="btn btn-outline-primary">Continue Shopping</a></p>
                           </div>
                        </div>';
                  }
               ?>
         </div>
      </div>
    </main>    

<?php } ?>