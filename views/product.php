<?php 

    session_start();
    $title = "Product";

    if(!isset($_GET['id'])) {
        header('location: catalog.php?AccessDenied');
    }

    include_once "../partials/template.php";
?>

<?php function get_content(){ ?>

    <main>
        <div class="container mt-5">
            <div class="row">
                <?php

                    include_once "../config/dbh.inc.php";

                    if(isset($_GET['id'])){
                        $id = $_GET['id'];
                        $sql = "SELECT * FROM `items` WHERE id = '$id'";
                    }else{
                        $id = "";
                    }   

                    $result = mysqli_query($conn, $sql);

                    $row = mysqli_fetch_assoc($result);
                ?>  
                    
                <div class="col-lg-3">
                    <img src="<?php echo $row['img_path']; ?>" alt="" width="100%" height="250px" style="border: 1px solid #ddd">
                </div>
                <div class="col-lg-9">
                    <h2>PRODUCT INFORMATION</h2>
                    <hr>
                    <h4><?php echo $row['name']; ?></h4>
                    <p><?php echo $row['description']; ?></p>
                    <h5>₱ <?php echo $row['price']; ?></h5>
                    <form method="POST" action="catalog.php?cat_id=<?php echo $id; ?>&message=success">
                        <div class="row">
                            <div class="col-3">
                                <?php
                                    if( isset($_SESSION["cart"]) )
                                    {
                                        // echo "<pre>";
                                        // print_r($_SESSION["cart"]);
                                        // echo "</pre>";

                                        foreach($_SESSION["cart"] as $key => $value)
                                        {

                                            // echo $value["product_id"];

                                            if($value["product_id"] == $id)
                                            {
                                                echo '<input type="number" class="form-control ml-3" min="1" max="10" value="'. $value["quantity"] .'" name="qty" id="qty">'; 
                                            }
                                            // else
                                            // {
                                            //     // echo '<input type="number" class="form-control ml-3" min="1" max="10" value="1" name="qty" id="qty">';
                                            // }
                                        }

                                        echo '<input type="number" class="form-control ml-3" min="1" max="10" value="1" name="qty" id="qty">';
                                    }
                                    // else 
                                    // {
                                    //     echo '<input type="number" class="form-control ml-3" min="1" max="10" value="1" name="qty" id="qty">';
                                    // }
                                ?>
                               <!--  <input type="number" class="form-control ml-3" min="1" max="10" value="1" name="qty" id="qty"> -->
                            </div>
                            <div class="col-3">
                                <button class="btn btn-block btn-primary" type="submit" name="submit"><i class="fas fa-shopping-cart"></i>&nbsp;Add to Cart</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>

<?php } ?>