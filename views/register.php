<?php 
    session_start();

    $title = "Register"; 
    require_once "../partials/template.php";
?>


<?php function get_content(){  ?>

    <main style="min-height: 66.5vh;">
        <div class="container">
            <div class="row mt-5">
                <div class="col col-md-8 col-sm-12 m-auto">
                    <fieldset>    	
                        <legend>USER REGISTRATION</legend>
                        <?php
                            if( isset( $_GET['message'] ) ){

                                $response = $_GET['message'];

                                if ( $response == "registerFailed" ){
                                    $response = "<div class='alert alert-success alert-dismissible fade show' role='alert'><i class='fas fa-exclamation-triangle'></i>&nbsp;Failed to register. 
                                                    <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                                        <span aria-hidden='true'>&times;</span>
                                                    </button>
                                                </div>";
                                }

                                if ( $response == "denied" ){
                                    $response = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><i class='fas fa-exclamation-triangle'></i>&nbsp;Access Denied! 
                                                    <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                                                        <span aria-hidden='true'>&times;</span>
                                                    </button>
                                                </div>";
                                }

                            }else{

                                $response = "";

                            }
                        ?>

                        <?php echo $response; ?>

                        <form action="../controllers/register_action.php" method="POST">
                            <input type="text" class="form-control" name="fname" placeholder="First Name" required>
                            <br>
                            <input type="text" class="form-control" name="lname" placeholder="Last Name" required>
                            <br>
                            <input type="email" class="form-control" name="email" placeholder="Email Address" required>
                            <br>
                            <input type="password" class="form-control" name="password" placeholder="Password" required>
                            <br>
                            <input type="text" class="form-control" name="address" placeholder="Address" required>
                            <br>
                            <button type="submit" name="submit" class="btn btn-block btn-primary"><i class="fas fa-sign-in-alt"></i>&nbsp;REGISTER</button>
                        </form>
                    </fieldset>	
                </div>
            </div>	
        </div>
    </main>

<?php } ?>