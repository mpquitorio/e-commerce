
let qtys = $('.qty');

for (qty of qtys){
    $(qty).on("change", updateQty);
}

function updateQty(){

    let newQty = $(this).val();
    //console.log(`The product quantity is: ${newQty}`);

    let price  = $(this).attr("data-price");
    //console.log(`The product price is: ${price}`);

    let product_id = $(this).attr("data-product-id");
    //console.log(`The product ID is: ${product_id}`);

    let product_pos = $(this).attr("data-product-pos");
    //console.log(`The product ID is: ${product_pos}`);

    let updatedValue = newQty * price; 

    let parent = $(this).parent();

    $(parent).next().children().html( number_format(updatedValue.toFixed(2)) );
    
    subTotal();

	/*----------  Update the SESSION["cart"] using Fetch API ----------*/

    //  Create Form Data to hold the values to pass to ../controllers/update.php
    let formData = new FormData();
    formData.append("qty", newQty);
    formData.append("product_pos", product_pos);
    formData.append("product_id", product_id);

    fetch("../controllers/update.php", {
    	method: 'POST',
    	body: formData
    })
    .then(function (response){
        return response.text();
    }).then(function(text){
        console.log(text);
    }).catch(function (error){
        console.log(error)
    })
    /*---------- End of Update  SESSION["cart"] using Fetch API ----------*/ 

}

function subTotal(){

    let productsSubTotal = $('.sub');

    let sum = 0;
    for(let i = 0; i < productsSubTotal.length; i++){
        sum += parseFloat( $(productsSubTotal[i]).html().replace(",","") );
    }
    $('#subtotal').html( number_format(sum.toFixed(2)) );
    $('#total').html( number_format(sum) );
}

function number_format(num) {
    let n = num.toString().split(".");
    n[0] = n[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    return n.join(".");
}

